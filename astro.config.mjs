import { defineConfig } from "astro/config";

// https://astro.build/config
export default defineConfig({
  sitemap: true,
  site: "https://rendiriz.gitlab.io/astro-example/",
  outDir: "public",
  publicDir: "static",
});
